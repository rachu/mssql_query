#! /usr/bin/python

import pyodbc
from contextlib import closing
import sys
import getopt

def parse_args(argv):
    dbuser = ''
    dbpassword = ''
    server = ''
    database = ''
    
    try:
        opts, args = getopt.getopt(argv, "hu:p:s:d:", ["dbuser=", "dbpass=", "server=","database="])
    except getopt.GetoptError:
        print('mssql_query -u <dbuser> -p <dbpassword> -s <server> -d <database>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('mssql_query -u <dbuser> -p <dbpassword> -s <server> -d <database>')
            sys.exit()
        elif opt in ("-u", "--dbuser"):
            dbuser = arg
        elif opt in ("-p", "--dbpass"):
            dbpassword = arg
        elif opt in ("-s", "--server"):
            server = arg
        elif opt in ("-d", "--database"):
            database = arg


    return dbuser,dbpassword,server,database

def main(argv):
    username,password,server,database = parse_args(argv)
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 13 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()


    try:
        with closing(cnxn.cursor()) as c:
            query = '''select AddressLine1,AddressLine2,City from person.Address'''
            c.execute(query)
            addresses = c.fetchall()

    except pyodbc.DatabaseError as e:
        print("Error reading database: ", e)
        addresses = None

    if addresses != None:
        for address in addresses:
            print(address[2])

if __name__ == "__main__":
   main(sys.argv[1:])