# Sample python program using the SQL Server driver

## Purpose

This sample program will connect to a sql server database and run a query.

It can be used to test your connectivity to SQL Server.

## Dependencies

* Python 3.x
* pyodbc
* odbc

## Execution

python mssql_query.py -u tempchu -p password -s localhost -d AdventurWorks2012

## References

* https://docs.microsoft.com/en-us/sql/connect/python/python-driver-for-sql-server
